<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Song;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SongController extends Controller
{
     /**
     * Create a new SongController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['allSongs']]);
    }

    public function index(){
        return User::with(['songs'])->where('id', Auth::id())->get();
    }

    public function genre() {
        return response()->json(Genre::all());
    }

    public function store(Request $request){
        
        $this->validate($request, [
            'title' => 'required',
            'artist' => 'required',
            'year_release' => 'required',
            'featured_image' => 'required',
            'featured_song' => 'required',
            'genre_id' => 'required'
        ]);

        Song::create([
            'title' => $request->title,
            'artist' => $request->artist,
            'year_release' => $request->year_release,
            'genre_id' => $request->genre_id,
            'featured_image' => $request->featured_image,
            'featured_song' => $request->featured_song,
            'user_id' => Auth::id(),
        ]);
           
        return response()->json(['msg' => 'Success'], 200);
    }

     
    public function update(Request $request, $id){
        
        $this->validate($request, [
            'title' => 'required',
            'artist' => 'required',
            'year_release' => 'required',
            'genre_id' => 'required',
            'featured_image' => 'required',
            'featured_song' => 'required'
        ]);

        $data = [
            'title' => $request->title,
            'artist' => $request->artist,
            'year_release' => $request->year_release,
            'featured_image' => $request->featured_image,
            'featured_song' => $request->featured_song,
            'genre_id' => $request->genre_id,
        ];

        Song::where('id', $id)->update($data);
    }

    public function destroy($id){
        $song = Song::where('id', $id)->first();
        $this->deleteFile($song['featured_image']);
        $this->deleteFile($song['featured_song']);
        Song::destroy($id);
    }


    public function allSongs(){
        return response()->json(Song::all());
    }

    public function deleteFile($fileName){
        $filePath = public_path().'/uploads/'.$fileName;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFile(Request $request){
        $fileName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);
        return $fileName;
    }
}
