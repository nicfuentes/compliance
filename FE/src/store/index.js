import Vue from 'vue'
import Vuex from 'vuex';
import songs from './modules/songs'
import auth from './modules/auth'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        songs
    }
});