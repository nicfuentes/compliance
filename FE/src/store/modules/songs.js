import axios from '@/axios'

const SONGS = "songs";

export default {
    state: {
        genres: [],
        songs: [],
        songsforHome: []
    },
    getters: {
        GET_SONGS(state) {
            return state.songs;
        },
        GET_SONGS_HOME(state) {
            return state.songsforHome;
        },
        GET_GENRES(state) {
            return state.genres;
        }
    },
    mutations: {
        SET_SONGS(state, songs) {
            state.songs = songs;
        },
        SET_GENRES(state, genres) {
            state.genres = genres;
        },
        SET_SONGS_HOME(state, songs) {
            state.songsforHome = songs;
        },
    },
    actions: {
        async STORE_SONG({ commit }, data) {
            const res = await axios.post(`${SONGS}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        async UPDATE_SONG({ commit }, { data, id }) {
            const res = await axios.put(`${SONGS}/${id}`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async DESTROY_SONG({ commit }, id) {
            const res = await axios.delete(`${SONGS}/${id}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        async FETCH_SONGS({ commit }) {
            await axios.get(`${SONGS}`)
                .then((response) => {
                    commit("SET_SONGS", response.data[0].songs);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_HOME_SONGS({ commit }) {
            await axios.get("allSongs")
                .then((response) => {
                    commit("SET_SONGS_HOME", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
        async FETCH_GENRES({ commit }) {
            await axios.get("genre")
                .then((response) => {
                    commit("SET_GENRES", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}